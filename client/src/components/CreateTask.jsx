import React, { useState } from "react";
import { Form, Input, Button, Select, message } from "antd";
import axios from "axios";
import { useAuth } from "../contexts/authContext";
import LoadingSpinner from "./Spinner";

const { TextArea } = Input;
const { Option } = Select;

const CreateTask = () => {
  const [form] = Form.useForm();
  const { currentUser } = useAuth();
  const [loading, setLoading] = useState(false);

  const onFinish = async (values) => {
    try {
      setLoading(true);
      await axios.post("https://icliniq.onrender.com/tasks", values, {
        headers: {
          Authorization: `Bearer ${currentUser.accessToken}`,
        },
      });
      message.success("Task created successfully");
      form.resetFields();
    } catch (error) {
      message.error("Failed to create task");
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="w-full">
      {loading && <LoadingSpinner />}
      <Form
        className="w-8/12 mx-auto"
        form={form}
        layout="vertical"
        onFinish={onFinish}
        initialValues={{ status: "pending" }}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[{ required: true, message: "Please input the title!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item name="description" label="Description">
          <TextArea rows={4} />
        </Form.Item>

        <Form.Item
          name="status"
          label="Status"
          rules={[{ required: true, message: "Please select a status!" }]}
        >
          <Select>
            <Option value="pending">Pending</Option>
            <Option value="in-progress">In Progress</Option>
            <Option value="completed">Completed</Option>
          </Select>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Create Task
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateTask;
