// src/components/LoadingSpinner.js
import React from "react";
import { Spin } from "antd";

const LoadingSpinner = () => {
  return (
    <div className=" fixed top-0 left-0 right-0 h-screen w-full flex items-center justify-center">
      <div className="text-center">
        <Spin size="large" />
        <div>initial loading may take upto 50 seconds</div>
      </div>
    </div>
  );
};

export default LoadingSpinner;
